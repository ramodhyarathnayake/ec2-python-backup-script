import boto3
import schedule

ec2_client = boto3.client('ec2', region_name = "ap-southeast-1")

def creat_snaps_daily():
    volumes = ec2_client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Environment',
                'Values': ['production']
            }
        ]
    )
    for volume in volumes['Volumes']:
        new_snapshot = ec2_client.create_snapshot(
            VolumeId=volume['VolumeId']
        )

    print(new_snapshot)

schedule.every().day.do(creat_snaps_daily)

while True:
    schedule.run_pending()